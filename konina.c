/*
   Derived from find_all.c, an example program from libusb,
   which is

   "This program is distributed under the GPL, version 2"

   For the changes:
   (c) 2016 Samsung Electronics
   Andrzej Pietrasiewicz <andrzej.p@samsung.com>

*/

#include <stdio.h>
#include <stdlib.h>
#include <ftdi.h>
#include <getopt.h>

#define write_bit_pattern(ftdic)					\
	do {								\
	    ftdi_set_bitmode(&(ftdic), bit_pattern, BITMODE_BITBANG);	\
            ftdi_write_data(&(ftdic), &bit_pattern, 1);		\
	} while (0);

#define MSG(...) 				\
	do {					\
		if (!silent)			\
			printf(__VA_ARGS__);	\
	} while (0)

static int dev_num = -1;
static struct ftdi_device_list *dev;
static int interface = INTERFACE_D;
static int list;
static int gpio1;
static int gpio2;
static int soft_boot;
static int soft_reset;
static int led2;
static int led3;
static int konina_reset;
static int vendor_id = 0x0403;
static int product_id = 0x6011;
static int silent;
static int eeprom_decode;
static int eeprom_update;
static unsigned char bit_pattern;
static const char *manufacturer;
static const char *product;
static const char *serial;
static char *new_manufacturer;
static char *new_product;
static char *new_serial;
static int new_vendor_id;
static int new_product_id;

static inline void usage(const char *argv0)
{
   fprintf(stderr,
           "Usage: %s [options]\n"
	   "\n"
           "Set and clear bits in interface D of konina board.\n"
	   "Bits which are not specified are cleared.\n"
	   "The set bits persist until konina is reset with --konina-reset.\n"
	   "After this program is used the host will not be able to properly\n"
	   "use serial transmission on konina's UART3 until --konina-reset.\n"
	   "\n"
	   "Options:\n"
	   "\n"
	   "\t--list|-l list all konina (FTDI) devices available,\n"
	        "\t\tcannot be used with any bit-setting options\n"
	   "\t--gpio1|-g set EXT_GPIO_1\n"
	   "\t--gpio2|-o set EXT_GPIO_2\n"
	   "\t--soft-boot|-b set SOFT_BOOT\n"
	   "\t--soft-reset|-r set SOFT_RESET for a short while, led3 blinks\n"
                "\t\ta number of times when this happens; if more bit setting\n"
	        "\t\toptions other than --konina-reset are specified this is the last\n"
	        "\t\toperation performed\n"
	   "\t--led2|-2 set LED_GREEN\n"
	   "\t--led3|-3 set LED_RED\n"
	   "\t--konina-reset|-k reset FTDI in konina, led2 blinks a number of times\n"
                "\t\twhen this happens; cannot be used with other bit setting options\n"
	        "\t\texcept --soft-reset, --konina-reset is the last operation performed\n"
	   "\t--help|-h print this message and exit, ignore any other options\n"
	   "\t--vendor-id|-v USB vendor ID of FTDI chip to use, 0x0403 by default\n"
	   "\t--product-id|-p USB product ID of FTDI chip to use, 0x6011 by default\n"
	   "\t--manufacturer|-m USB manufacturer string\n"
	   "\t--product|-u USB product string\n"
	   "\t--serial|-e USB serial number string\n"
	   "\t--device|-d index in the list of devices detected, can be omitted if only one\n"
                "\t\tkonina is connected to the host - then assumed 0\n"
           "\t--silent|-s don't print anything to standard output\n"
	   "\n"
	   "Expert options:\n"
	   "\n"
	   "\t--eeprom-decode|-c read and decode EEPROM contents; cannot be used with bit\n"
	   "\t\tsetting options\n"
	   "\t--eeprom-update|-a update EEPROM contents; cannot be used with bit\n"
	   "\t\tsetting options but at least one of --new-vendor-id --new-product-id\n"
	   "\t\t--new-manufacturer --new-product --new-serial must be used\n"
	   "\t--new-vendor-id|-n USB vendor ID to set; use only with --eeprom-update\n"
	   "\t--new-product-id|-b USB product ID to set; use only with --eeprom-update\n"
	   "\t--new-manufacturer|-a USB manufacturer string to set; use only with\n"
	   "\t\t--eeprom-update\n"
	   "\t--new-product|-f USB product string to set; use only with --eeprom-update\n"
	   "\t--new-serial|-i USB serial number string to set; use only with\n"
	   "\t\t--eeprom-update\n"
           "\n"
	   "Derived from libusb find_all.c by Andrzej Pietrasiewicz <andrzej.p@samsung.com>\n"
           "\n",
           argv0);
   exit(EXIT_FAILURE);
}

static inline void set_pattern(void)
{
	bit_pattern = 0;
	if (gpio1)
		bit_pattern |= (1 << 2);
	if (gpio2)
		bit_pattern |= (1 << 3);
	if (soft_boot)
		bit_pattern |= (1 << 4);
	if (soft_reset)
		bit_pattern |= (1 << 5);
	if (led2)
		bit_pattern |= (1 << 6);
	if (led3)
		bit_pattern |= (1 << 7);
}

static struct ftdi_device_list *find_dev(struct ftdi_context *ftdic, struct ftdi_device_list *devlist, int devs,
		const char *manuf, const char *prod, const char *ser)
{
    char manufacturer[128], product[128], serial[128];
    struct ftdi_device_list *curdev, *dev = NULL;
    int ret, i = 0;

    for (curdev = devlist; curdev != NULL; curdev = curdev->next)
    {
        uint8_t bus, addr;

        if ((ret = ftdi_usb_get_strings(ftdic, curdev->dev, manufacturer, 128, NULL, 0, NULL, 0)) < 0)
        {
            fprintf(stderr, "ftdi_usb_get_strings failed: %d (%s)\n", ret, ftdi_get_error_string(ftdic));
            return NULL;
        }
	if (manuf && strcmp(manufacturer, manuf))
            continue;

        if ((ret = ftdi_usb_get_strings(ftdic, curdev->dev, NULL, 0, product, 128, NULL, 0)) < 0)
        {
            fprintf(stderr, "ftdi_usb_get_strings failed: %d (%s)\n", ret, ftdi_get_error_string(ftdic));
            return NULL;
        }
	if (prod && strcmp(product, prod))
            continue;

        if (ser && (ret = ftdi_usb_get_strings(ftdic, curdev->dev, NULL, 0, NULL, 0, serial, 128)) < 0)
        {
	    fprintf(stderr, "\n");
            continue;
        }
	if (ser && strcmp(serial, ser))
            continue;

        MSG("Found device: %d\n", i);

	bus = libusb_get_bus_number(curdev->dev);
	addr = libusb_get_device_address(curdev->dev);
        MSG("[bus:%d address:%d] Manufacturer: %s, Product: %s\n\n", bus, addr, manufacturer, product);
	if (!dev && (dev_num == -1 || i == dev_num))
		dev = curdev;
	i++;
    }

    if (i > 1 && dev_num == -1)
        dev = NULL;

    return dev;
}

static int decode_eeprom(struct ftdi_context *ftdic)
{
    int ret;

    if ((ret = ftdi_read_eeprom(ftdic)) < 0) {
        fprintf(stderr, "Cannot read EEPROM\n");
	return EXIT_FAILURE;
    }

    if ((ret = ftdi_eeprom_decode(ftdic, !silent)) < 0) {
        fprintf(stderr, "Cannot decode EEPROM\n");
	return EXIT_FAILURE;
    }
}

static int update_eeprom(struct ftdi_context *ftdic, char *manuf, char *prod, char *ser, int v_id, int p_id)
{
    int ret;

    if ((ret = ftdi_read_eeprom(ftdic)) < 0) {
        fprintf(stderr, "Cannot read EEPROM\n");
	return EXIT_FAILURE;
    }

    if ((ret = ftdi_eeprom_initdefaults(ftdic, manuf, prod, ser)) < 0) {
        fprintf(stderr, "Cannot set default EEPROM values\n");
	return EXIT_FAILURE;
    }

    if (v_id > 0 && (ret = ftdi_set_eeprom_value(ftdic, VENDOR_ID, v_id)) < 0) {
        fprintf(stderr, "Cannot set vendor id in EEPROM\n");
	return EXIT_FAILURE;
    }

    if (p_id > 0 && (ret = ftdi_set_eeprom_value(ftdic, PRODUCT_ID, p_id)) < 0) {
        fprintf(stderr, "Cannot set product id in EEPROM\n");
	return EXIT_FAILURE;
    }

    if ((ret = ftdi_eeprom_build(ftdic)) < 0) {
        fprintf(stderr, "Cannot build EEPROM binary image\n");
	return EXIT_FAILURE;
    }

    if ((ret = ftdi_write_eeprom(ftdic)) < 0) {
        fprintf(stderr, "Cannot write EEPROM binary image\n");
	return EXIT_FAILURE;
    }
}

int main(int argc, char *argv[])
{
    int ret, i, devs;
    struct ftdi_context ftdic;
    struct ftdi_device_list *devlist;

    while (1) {
        static struct option long_options[] = {
            {"list",       no_argument, 0, 'l'},
            {"gpio1",      no_argument, 0, 'g'},
            {"gpio2",      no_argument, 0, 'o'},
            {"soft-boot",  no_argument, 0, 'b'},
            {"soft-reset", no_argument, 0, 'r'},
            {"led2",       no_argument, 0, '2'},
            {"led3",       no_argument, 0, '3'},
            {"konina-reset", no_argument, 0, 'k'},
            {"help",       no_argument, 0, 'h'},
	    {"vendor-id",  required_argument, 0, 'v'},
	    {"product-id", required_argument, 0, 'p'},
	    {"manufacturer", required_argument, 0, 'm'},
	    {"product",    required_argument, 0, 'u'},
	    {"serial",     required_argument, 0, 'e'},
	    {"device",     required_argument, 0, 'd'},
	    {"silent",     no_argument, 0, 's'},
	    {"eeprom-decode", no_argument, 0, 'c'},
	    {"eeprom-update", no_argument, 0, 't'},
	    {"new-vendor-id", required_argument, 0, 'n'},
	    {"new-product-id", required_argument, 0, 'j'},
	    {"new-manufacturer", required_argument, 0, 'a'},
	    {"new-product", required_argument, 0, 'f'},
	    {"new-serial", required_argument, 0, 'i'},
            {0,            0,           0,  0 }
        };
	int option_index = 0;

        i = getopt_long(argc, argv, "lgohbr23ksctv:p:m:u:e:d:n:j:a:f:i:", long_options, &option_index);
	if (i == -1)
		break;

        switch (i) {
	case 'l':
	    list = 1;
	    break;
        case 'g':
	    gpio1 = 1;
            break;
        case 'o':
	    gpio2 = 1;
            break;
        case 'b':
	    soft_boot = 1;
            break;
        case 'r':
	    soft_reset = 1;
            break;
        case '2':
	    led2 = 1;
            break;
	case '3':
	    led3 = 1;
            break;
	case 'k':
	    konina_reset = 1;
	    break;
	case 'h':
	    usage(argv[0]);
	    break;
        case 'v':
            vendor_id = (int)strtol(optarg, NULL, 0);
	    break;
        case 'p':
            product_id = (int)strtol(optarg, NULL, 0);
	    break;
	case 'm':
            manufacturer = optarg;
	    break;
        case 'u':
            product = optarg;
            break;
        case 'e':
            serial = optarg;
            break;
	case 'd':
	    dev_num = atoi(optarg);
	    if (dev_num < 0)
	        usage(argv[0]);
	    break;
	case 's':
            silent = 1;
	    break;
        case 'c':
            eeprom_decode = 1;
	    break;
        case 't':
            eeprom_update = 1;
	    break;
        case 'n':
            new_vendor_id = (int)strtol(optarg, NULL, 0);
            break;
        case 'j':
            new_product_id = (int)strtol(optarg, NULL, 0);
            break;
	case 'a':
            new_manufacturer = optarg;
	    break;
	case 'f':
            new_product = optarg;
	    break;
	case 'i':
            new_serial = optarg;
	    break;
	case '?':
            usage(argv[0]);
        default:
            usage(argv[0]);
        }
    }

    if ((gpio1 || gpio2 || soft_boot || led2 || led3) && (konina_reset))
	usage(argv[0]);

    if ((gpio1 || gpio2 || soft_boot || led2 || led3 || konina_reset || soft_reset) && list)
        usage(argv[0]);

    if ((gpio1 || gpio2 || soft_boot || led2 || led3 || konina_reset || soft_reset || list) && (eeprom_decode || eeprom_update))
        usage(argv[0]);

    if ((new_vendor_id || new_product_id || new_manufacturer || new_product || new_serial) && (!eeprom_decode && !eeprom_update))
        usage(argv[0]);

    if (eeprom_update && (!new_vendor_id && !new_product_id && !new_manufacturer && !new_product && !new_serial))
        usage(argv[0]);

    if (ftdi_init(&ftdic) < 0)
    {
        fprintf(stderr, "ftdi_init failed\n");
        return EXIT_FAILURE;
    }

    MSG("Looking for FTDI devices with VID:%04x PID:%04x\n", vendor_id, product_id);
    if ((devs = ftdi_usb_find_all(&ftdic, &devlist, vendor_id, product_id)) < 0)
    {
        fprintf(stderr, "ftdi_usb_find_all failed: %d (%s)\n", ret, ftdi_get_error_string(&ftdic));
        return EXIT_FAILURE;
    }

    MSG("Number of FTDI devices found: %d\n", devs);
    if (devs < 1) {
    	fprintf(stderr, "no devices found, exiting\n");
	return EXIT_FAILURE;
    }

    dev = find_dev(&ftdic, devlist, devs, manufacturer, product, serial);

    if (list)
        return EXIT_SUCCESS;

    if (dev == NULL) {
	fprintf(stderr, "Which device to use?\n");
	return EXIT_FAILURE;
    }

    ftdi_set_interface(&ftdic, interface);

    if ((ret = ftdi_usb_open_dev(&ftdic, dev->dev)) < 0)
    {
        fprintf(stderr, "unable to open ftdi device: %d (%s)\n", ret, ftdi_get_error_string(&ftdic));
        return EXIT_FAILURE;
    }

    if (eeprom_decode) {
        if ((ret = decode_eeprom(&ftdic)) < 0) {
            fprintf(stderr, "Decoding EEPROM failed\n");
	    return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
    }

    if (eeprom_update) {
        if ((ret = update_eeprom(&ftdic, new_manufacturer, new_product, new_serial, new_vendor_id, new_product_id)) < 0) {
            fprintf(stderr, "Updating EEPROM failed\n");
	    return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
    }

    set_pattern();
    write_bit_pattern(ftdic);
    if (soft_reset) {
        usleep(100000);
	soft_reset = 0;
	for (i = 0; i < 8; ++i) {
            led3 ^= 1;
            set_pattern();
            write_bit_pattern(ftdic);
            usleep(50000);
	}
    }
    if (konina_reset) {
	for (i = 0; i < 8; ++i) {
            led2 ^= 1;
            set_pattern();
            write_bit_pattern(ftdic);
            usleep(50000);
	}
        ftdi_disable_bitbang(&ftdic);
    }
    if ((ret = ftdi_usb_reset(&ftdic)) < 0)
    {
	fprintf(stderr, "Unable to reset ftdi device: %d (%s)\n", ret, ftdi_get_error_string(&ftdic));
	return EXIT_FAILURE;
    }
    ftdi_usb_close(&ftdic);

    ftdi_list_free(&devlist);
    ftdi_deinit(&ftdic);

    return EXIT_SUCCESS;
}
