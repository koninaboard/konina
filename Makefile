konina: konina.c build
	gcc -I usr/include/libftdi1/ -L usr/lib/ konina.c -Wl,-Bstatic -lftdi1 -lusb-1.0 -Wl,-Bdynamic -lpthread -ludev -okonina

.PHONY: build
ftdi: build
	mkdir -p build
	cd build && cmake -DCMAKE_INSTALL_PREFIX=../usr ../libftdi/ && make install

clean:
	rm -rf konina

clean_ftdi:
	rm -rf build
